//
//  FiniteAutomataTests.swift
//  proj1Tests
//
//  Created by Filip Klembara on 17/02/2020.
//

import XCTest
import Simulator
import FiniteAutomata
import MyFiniteAutomatas

class FiniteAutomataTests: XCTestCase {
    func testAstarC2Bstar() throws {
        let fa = AstarC2Bstar.data
        let f = try JSONDecoder().decode(FiniteAutomata.self, from: fa)

        let sim = Simulator(finiteAutomata: f)
        AstarC2Bstar.valid.forEach { input, states in
            do {
                let r = try sim.simulate(on: input)

                XCTAssertGreaterThan(r.count, 0)
                XCTAssertFalse(r.isEmpty)
                XCTAssertEqual(r, states, "generated array of states '\(r)' for input '\(input)' is not equal to expected '\(states)'")
            }
            catch {
                XCTFail("Simulation failed with an error")
            }
        }

        AstarC2Bstar.invalid.forEach { input in
            do {
                let r = try sim.simulate(on: input)

                XCTAssertEqual(r.count, 0)
                XCTAssertTrue(r.isEmpty)
            }
            catch {
            
            }
        }
    }

    func testCIdentifierAutomata() throws {
        let fa = CIdentifierAutomata.description.description.data(using: .utf8)!
        let f = try JSONDecoder().decode(FiniteAutomata.self, from: fa)

        let sim = Simulator(finiteAutomata: f)
        CIdentifierAutomata.valid.forEach { input, states in
            do {
                let r = try sim.simulate(on: input)

                XCTAssertGreaterThan(r.count, 0)
                XCTAssertFalse(r.isEmpty)
                XCTAssertEqual(r, states, "generated array of states '\(r)' for input '\(input)' is not equal to expected '\(states)'")
            }
            catch {
                XCTFail("Simulation failed with na error")
            }
        }

        CIdentifierAutomata.invalid.forEach { input in
            do {
                let r = try sim.simulate(on: input)
                XCTAssertEqual(r.count, 0)
                XCTAssertTrue(r.isEmpty)
            }
            catch {
                
            }
        }
    }

    static var allTests = [
    ("testAstarC2Bstar", testAstarC2Bstar),
    ("testCIdentifierAutomata", testCIdentifierAutomata),
    ]
}
