//
//  FiniteAutomata.swift
//  FiniteAutomata
//
//  Created by Filip Klembara on 17/02/2020.
//

/// Finite automata
public struct FiniteAutomata {
    public let states : [String]
    public let symbols : [String]
    
    public struct Transition : Decodable {
        public let with : String
        public let to : String
        public let from : String
    }
    
    public let transitions : [Transition]
    public let initialState : String
    public let finalStates : [String]
    
    /// Gets next state
    /// - Parameter state: current state
    /// - Parameter input
    /// - Returns: next state (`string`) from transition or `nil` if no valid transition is found
    public func getNextState(state : String, input : String) -> String? {
        
        // Filter transitions to only the ones that goes from the current state with the current input
        let availableTransitions = self.transitions.filter { transition in
            return
                transition.from == state
                && transition.with == input
        }
        
        if (availableTransitions.count > 0) {
            return availableTransitions[0].to
        }
        
        // Did not find a valid transition
        return nil
    }
}


extension FiniteAutomata: Decodable {
    
    // Checks the automata definition for invalid states
    public var validStates : Bool {
        // Check initialState
        if (!self.states.contains(self.initialState)) {
            return false
        }
        // Check transitions
        for transition in self.transitions {
            if (!self.states.contains(transition.from) || !self.states.contains(transition.from)) {
                return false
            }
        }
        // Check final states
        for state in self.finalStates {
            if (!self.states.contains(state)) {
                return false
            }
        }
        return true
    }
    
    // Checks the automata definition for invalid symbols in transition
    public var validSymbols : Bool {
        // Check transitions
        for transition in self.transitions {
            if (!self.symbols.contains(transition.with)) {
                return false
            }
        }
        return true
    }
}
