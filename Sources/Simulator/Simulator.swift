//
//  Simulator.swift
//  Simulator
//
//  Created by Filip Klembara on 17/02/2020.
//

import Foundation
import FiniteAutomata

/// Simulator
public struct Simulator {
    /// Finite automata used in simulations
    private let finiteAutomata: FiniteAutomata

    /// Initialize simulator with given automata
    /// - Parameter finiteAutomata: finite automata
    public init(finiteAutomata: FiniteAutomata) {
        self.finiteAutomata = finiteAutomata
    }

    /// Simulate automata on given string
    /// - Parameter string: string with symbols separated by ','
    /// - Returns: Empty array if given string is not accepted by automata,
    ///     otherwise array of states
    public func simulate(on string: String) throws -> [String] {
        
        // Check automata validity
        if (!self.finiteAutomata.validStates) {
            throw SimulatorError.undefinedState
        }
        if (!self.finiteAutomata.validSymbols) {
            throw SimulatorError.undefinedSymbol
        }
        
        let inputs = string.split(separator: ",").map(String.init)
        var states : [String] = []
        var currentState : String? = self.finiteAutomata.initialState
        
        states.append(currentState!) // Append initial state (not nil => no need to check)
        
        // Loop through all inputs
        for input in inputs {
            
            // Check for invalid input
            if (!self.finiteAutomata.symbols.contains(input)) {
                throw SimulatorError.invalidInput
            }
            
            currentState = self.finiteAutomata.getNextState(state: currentState!, input: input)
            if (currentState == nil) {
                // Current (input, state) combination does not have a valid transition
                return []
            }
            
            states.append(currentState!) // Append next state (not nil - already checked)
        }
        
        // Check if the last state is final
        if (self.finiteAutomata.finalStates.contains(states[states.count - 1])) {
            return states
        }
        
        return []
    }
}
