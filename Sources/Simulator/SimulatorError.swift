//
//  SimulatorError.swift
//  Simulator
//
//  Created by Tomáš Vojík on 29.03.2021.
//

import Foundation


/// Error thrown during automata simulation
public enum SimulatorError: Error {
    case invalidInput
    case undefinedState
    case undefinedSymbol
    case genericError
}

// MARK:- Description of error
extension SimulatorError: CustomStringConvertible {
    public var description: String {
        switch self {
        case .invalidInput:
            return "Invalid input string"
        case .undefinedState:
            return "Undefined state"
        case .undefinedSymbol:
            return "Undefined symbol"
        case .genericError:
            return "Generic"
        }
    }
}
