//
//  main.swift
//  proj1
//
//  Created by Filip Klembara on 17/02/2020.
//

import Foundation
import FiniteAutomata
import Simulator

// MARK: - Main
func main() -> Result<Void, RunError> {
    
    if (CommandLine.argc != 3) {
        return Result.failure(.invalidArguments)
    }
    let input : String = CommandLine.arguments[1]
    let file : String = CommandLine.arguments[2]
    
    let content : Data
    
    do {
        content = try String(contentsOfFile: file).data(using: .utf8)!
    }
    catch {
        return Result.failure(.fileError)
    }
    
    do {
        // Read file content
        let automata = try JSONDecoder().decode(FiniteAutomata.self, from: content)
        let simulator = Simulator(finiteAutomata: automata)
        let states = try simulator.simulate(on: input)
        if (states.count == 0) {
            return Result.failure(.invalidInput)
        }
        printResult(states)
    }
    catch SimulatorError.invalidInput {
        return Result.failure(.invalidInput)
    }
    catch SimulatorError.undefinedSymbol {
        return Result.failure(.undefinedSymbol)
    }
    catch SimulatorError.undefinedState {
        return Result.failure(.undefinedState)
    }
    catch SimulatorError.genericError {
        return Result.failure(.genericError)
    }
    catch is DecodingError {
        return Result.failure(.decodeError)
    }
    catch {
        return Result.failure(.genericError)
    }
    return Result.success(())
}

// Print automata simulation result to stdout
func printResult(_ states : [String]) -> Void {
    for state in states {
        print(state);
    }
}

// MARK: - program body
let result = main()

switch result {
case .success:
    break
case .failure(let error):
    var stderr = STDERRStream()
    print("Error:", error.description, to: &stderr)
    exit(Int32(error.code))
}
