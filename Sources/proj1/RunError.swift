//
//  RunError.swift
//  proj1
//
//  Created by Filip Klembara on 17/02/2020.
//

enum RunError: Error {
    case notImplemented
    case invalidInput
    case invalidArguments
    case fileError
    case decodeError
    case undefinedState
    case undefinedSymbol
    case genericError
}

// MARK: - Return codes
extension RunError {
    var code: Int {
        switch self {
        case .notImplemented:
            return 66
        case .invalidArguments:
            return 11
        case .invalidInput:
            return 6
        case .fileError:
            return 12
        case .decodeError:
            return 20
        case .undefinedState:
            return 21
        case .undefinedSymbol:
            return 22
        case .genericError:
            return 99
        }
    }
}

// MARK:- Description of error
extension RunError: CustomStringConvertible {
    var description: String {
        switch self {
        case .notImplemented:
            return "Not implemented"
        case .invalidArguments:
            return "Invalid argunemts"
        case .invalidInput:
            return "Invalid input string"
        case .fileError:
            return "Error while working with file"
        case .decodeError:
            return "Cannot decode JSON file"
        case .undefinedState:
            return "Undefined state"
        case .undefinedSymbol:
            return "Undefined symbol"
        case .genericError:
            return "Generic"
        }
    }
}
